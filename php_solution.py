from php import BaseCar


class GasCar(BaseCar):

    def drive(self):
        return 'brrrum'

    def __add__(self, other):
        raise CarAccident()


class DieselCar(BaseCar):

    def drive(self):
        return 'pyr pyr pyr'

    def __add__(self, other):
        raise CarAccident()


class CarAccident(Exception):

    def __init__(self, msg=None):
        if msg is None:
            msg = 'Crash!'
        super().__init__(msg)
